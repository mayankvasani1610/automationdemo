package FunctionLibrary;

import java.io.IOException;
//import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;

//import com.dci.Acumen.CommonClassesReusable.AGlobalComponents;
//import com.dci.Acumen.CommonClassesReusable.ReadDataFromPropertiesFile;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import TestngRunner.ConfigFile;

public class BrowserSelection  {
	
	protected static WebDriver driver;
//	public static WebDriver default_driver;
//	public static WebDriver PrivateBrowser_driver2;
//	public static WebDriver PrivateBrowser_driver3;
//	public static WebDriver globalSeleniumInstance;
	
	public static ExtentReports report = new ExtentReports(System.getProperty("user.dir") + "\\Reporting\\report.html",true);
	public static ExtentTest logger;
	public static String imgeHtmlPath;
	
//	public static Dimension windowSize;
//	public static int punchDay = 0;

	
	
	//  @BeforeSuite
    @BeforeClass
	public static WebDriver beforeSuite() throws IOException {
    	
	//	logger = report.startTest("Browser initialization", "initialization of browser and open browser");
 
    	
		String gerkoPath = System.getProperty("user.dir");
		gerkoPath = gerkoPath + "\\Browser_Files\\geckodriver.exe";
		System.setProperty("webdriver.firefox.marionette", gerkoPath);
		System.out.println("Browser select " + ConfigFile.browserName);
	//	logger.log(LogStatus.INFO, "Browser select : "+AGlobalComponents.browserName);
		
		switch (ConfigFile.browserName) {
		

		case "MF":
			System.out.println("Running FireFox");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().window().setSize(new Dimension(1550, 838));
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			System.out.println("Mozilla firefox open sucessfully");
	//		logger.log(LogStatus.PASS, "Mozilla firefox open sucessfully");
			break;

		case "GC":

			System.setProperty("webdriver.chrome.driver", "Browser_Files/chromedriver.exe");
//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//		    ChromeOptions options = new ChromeOptions();
//		    options.addArguments("incognito");
//		    options.addArguments("--start-maximized");
//		    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		    driver = new ChromeDriver();
//		    driver.get("chrome://extensions-frame");
//			WebElement checkbox = driver.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
//			if (!checkbox.isSelected()) {
//			    checkbox.click();
//			}  
		    driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			System.out.println("Google Chrome launched successfully");
			break;
	

		case "IE":
//			System.setProperty("webdriver.ie.driver", "Browser_Files/IEDriverServer.exe");
//			DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
//			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
//			driver = new InternetExplorerDriver(dc);
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			driver.manage().window().maximize();
//			driver.manage().deleteAllCookies();
//			System.out.println("Internet explorer open sucessfully");
//			logger.log(LogStatus.PASS, "Internet explorer open sucessfully");
//			break;

		case "AS":
//			System.setProperty("webdriver.safari.driver", "Browser_Files\\SafariSetup.exe");
//			System.out.println("Running Safari");
//			driver = new SafariDriver();
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			driver.manage().window().maximize();
//			driver.manage().deleteAllCookies();
//			System.out.println("Safari open sucessfully");
//			logger.log(LogStatus.PASS, "Safari open sucessfully");
//			break;

//		default:
//			System.out.println("Running FireFox");
//			System.setProperty("webdriver.firefox.marionette", gerkoPath);
//			driver = new FirefoxDriver();
//			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//			driver.manage().window().setSize(new Dimension(1550, 840));
//			driver.manage().window().maximize();
//			driver.manage().deleteAllCookies();
//			System.out.println("Mozilla firefox open sucessfully");
//			break;
		}
		return driver;
	}

    @AfterMethod
    protected void afterMethod(ITestResult result) throws IOException
	{
    	switch (result.getStatus())
    	{
    	case 1:// Pass
			System.out.println("PASSED---"+result.getMethod().getMethodName());
			logger.log(LogStatus.PASS, "Test passed sucessfully");
			break;

		case 2:// Fail
			String ErrMsg = result.getThrowable().getMessage();
			String actionInfo = result.getName();
			if (ErrMsg.contains("expected [true]"))
			{
				System.out.println("FAILED---"+result.getMethod().getMethodName()+"------------->"+ErrMsg);
				logger.log(LogStatus.FAIL, "BUGGG >> " + ErrMsg);
				if(ErrMsg.contains("ASSERTIONERROR"))
				{
					logger.log(LogStatus.FAIL, "Screenshort of BUGG :" + imgeHtmlPath);
				}
				else
				{
				Utility.takeScreenshot(actionInfo.replaceAll(" ", "_"));
				logger.log(LogStatus.FAIL, "Screenshort of BUGG :" + imgeHtmlPath);
				}
				break;
			}
			else if (ErrMsg.contains("RUNTIME_ERROR"))
			{
				System.out.println("ERROR---"+result.getMethod().getMethodName()+"------------->"+ErrMsg);
				logger.log(LogStatus.ERROR, "" + ErrMsg);
				Utility.takeScreenshot(actionInfo.replaceAll(" ", "_"));
				logger.log(LogStatus.ERROR, "Screenshort of error :" + imgeHtmlPath);
				System.out.println("ERROR--Test error sucessfully"+result.getTestName());
				break;
			}
			else
			{
				System.out.println("ERROR---"+result.getMethod().getMethodName()+"------------->"+ErrMsg);
				logger.log(LogStatus.ERROR, "RUNTIME ERROR >> " + ErrMsg);
				break;
			}
		}
		report.endTest(logger);
		report.flush();
		}
	
	public void close()
	{
		driver.close();
	}

	@AfterSuite
	public void quitDriver() throws IOException, Throwable
	{
		
		Thread.sleep(4000);
		System.out.println("Successfully: Quit Browser");
//		File source = new File(System.getProperty("user.dir") + "\\ExtentReport\\Output");
//		File dest = new File(System.getProperty("user.dir") + "\\ExtentReport\\Output"+AGlobalComponents.currentDateLoggr);
//	    FileUtils.copyDirectory(source, dest);
	    driver.get("file://"+System.getProperty("user.dir") + "\\Reporting\\report.html") ;
		driver.findElement(By.xpath("//nav/ul/li[@class='theme-selector']")).click();
//		driver.quit();
	}
}
