package FunctionLibrary;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Utility extends BrowserSelection {
	
	public static void takeScreenshot(String SSname) {
		
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			
			//String screenShotPath=System.getProperty("user.dir") +"\\ExtentReport\\Output\\Screenshots\\";
			FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") +"\\Reporting\\Screenshots\\"+SSname+".png")); 
			imgeHtmlPath=logger.addScreenCapture((System.getProperty("user.dir") +"\\Reporting\\Screenshots\\"+SSname+".png").replace(System.getProperty("user.dir") +"\\Reporting\\Screenshots\\", "Screenshots\\").replace("file:///", "").replace("<img", "<img width=\"150\" height=\"70\""));                                
                   
	      
		} catch (IOException e) {
			throw new java.lang.RuntimeException("RUNTIME_ERROR : : Exception occur during take ScreenShot: "+SSname);
		}
	}
	public static String getCurrentDateTime (String format){
	    
		String timeStamp = new SimpleDateFormat(format).format(new Date());
	    return timeStamp;
	
	}
	
	public static String getCurrentWorkingDirectory(){
		
		String workingDir = System.getProperty("user.dir");
		return workingDir;
		}

	public static String getTitle(){
		
		dynamicwait.WAitUntilPageLoad();
		String getTitle=driver.getTitle();
		return getTitle;
	}
	
	public static void openUrl(String URL){
		
		dynamicwait.WAitUntilPageLoad();
		driver.get(URL);
	}
	public static String UniqueString(int length){
		
		String UniqueString=RandomStringUtils.randomAlphabetic(length);
		return UniqueString;
	}
	
    public static String UniqueNumber(int length){
		
		String UniqueNumber=RandomStringUtils.randomNumeric(length);
		return UniqueNumber;
	}
    
    public String getClassName(){
          
    	Class<?> enclosingClass = getClass().getEnclosingClass();
          if (enclosingClass != null) {
            System.out.println(enclosingClass.getName());
            return enclosingClass.getName();
            } else {
              System.out.println(getClass().getName());
              return getClass().getName();
    }
}
    public static void pageScrollDown(int length) throws InterruptedException{
    	JavascriptExecutor jse=(JavascriptExecutor) driver;
    	dynamicwait.WAitUntilPageLoad();
    	jse.executeScript("window.scrollBy(0,"+length+")", "");
    	dynamicwait.WAitUntilPageLoad();
    	Thread.sleep(1000);
    }
    public static void pageScrollUp(int length) throws InterruptedException{
    	JavascriptExecutor jse=(JavascriptExecutor) driver;
    	dynamicwait.WAitUntilPageLoad();
    	jse.executeScript("window.scrollBy(0,"+"-"+length+")", "");
    	dynamicwait.WAitUntilPageLoad();
    	Thread.sleep(1000);
    } 
    public static void pageScrollDownUsingRobot() throws AWTException, InterruptedException{
    	
    	dynamicwait.WAitUntilPageLoad();
    	Thread.sleep(1000);
    	Robot rb=new Robot();
    //	Thread.sleep(2000);
    	rb.keyPress(KeyEvent.VK_PAGE_DOWN);
    	Thread.sleep(3000);
    	rb.keyRelease(KeyEvent.VK_PAGE_DOWN);
    	Thread.sleep(200);
    } 
    public static void pageScrollUpUsingRobot() throws AWTException, InterruptedException{
    	
    	dynamicwait.WAitUntilPageLoad();
    	Thread.sleep(1000);
    	Robot rb=new Robot();
  //  	Thread.sleep(2000);
    	rb.keyPress(KeyEvent.VK_PAGE_UP);
    	Thread.sleep(3000);
    	rb.keyRelease(KeyEvent.VK_PAGE_UP);
    	Thread.sleep(200);
    } 
    
//    public static WebDriver openPrivateBrowser(){
//    	
//    	switch (AGlobalComponents.browserName) {
//    	
//    	case "MF":
//       		FirefoxProfile firefoxProfile = new FirefoxProfile();    
//    		firefoxProfile.setPreference("browser.privatebrowsing.autostart", true);
//    		PrivateBrowser_driver2 = new FirefoxDriver(firefoxProfile); 
//    		PrivateBrowser_driver2.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			PrivateBrowser_driver2.manage().window().maximize();
//			PrivateBrowser_driver2.manage().deleteAllCookies();
//			break;
//
//		case "GC":
//			System.setProperty("webdriver.chrome.driver", "Browser_Files/chromedriver.exe");
//			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//		    ChromeOptions options = new ChromeOptions();
//		    options.addArguments("incognito");
//		    options.addArguments("--start-maximized");
//		    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//		    PrivateBrowser_driver2 = new ChromeDriver(capabilities);
//		    PrivateBrowser_driver2.get("chrome://extensions-frame");
//			WebElement checkbox = PrivateBrowser_driver2.findElement(By.xpath("//label[@class='incognito-control']/input[@type='checkbox']"));
//			if (!checkbox.isSelected()) {
//			    checkbox.click();
//			}
//		//	PrivateBrowser_driver2.manage().window().maximize();
//		//	driver.manage().window().setSize(new Dimension(1040,784));
//			PrivateBrowser_driver2.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		//	PrivateBrowser_driver2.manage().window().setSize(new Dimension(1040,784));
//		   	break;
//			
//		default:
//			FirefoxProfile firefoxProfile2 = new FirefoxProfile();    
//    		firefoxProfile2.setPreference("browser.privatebrowsing.autostart", true);
//    		PrivateBrowser_driver2 = new FirefoxDriver(firefoxProfile2); 
//    		PrivateBrowser_driver2.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			PrivateBrowser_driver2.manage().window().maximize();
//			PrivateBrowser_driver2.manage().deleteAllCookies();
//			break;
//			
//    	}	
//    	return PrivateBrowser_driver2;
//	
  //  }
    public static void uploadFile(String fileLocation) throws AWTException, InterruptedException {
      
        	
            StringSelection stringSelection = new StringSelection(fileLocation);
  		    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
        
            Robot robot = new Robot();
            Thread.sleep(3000);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            Thread.sleep(2000);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(1000);
          
    }

}