package Module;

import org.testng.Assert;
import org.testng.annotations.Test;

import FunctionLibrary.BrowserSelection;
import FunctionLibrary.ByXpath;
import ObjectRepoistory.LoginPage;

public class Flightbooking extends BrowserSelection {

	
	@Test
	public static void testcase1(){
		
		logger = report.startTest("passchecking", "checking browser open");
		driver.get("https://www.facebook.com/");
		
//		ByXpath.enterTxt(LoginPage.userName, "surah@2357", actionInfo);
		
		ByXpath.enterTxt(LoginPage.userName, "suraj@gmail.com", "enter username");
		ByXpath.enterTxt(LoginPage.password, "1234gg", "enter password");
		ByXpath.click(LoginPage.loginBtn, "click on login btn");
	//	Assert.assertTrue(false);
		
	}
	
	@Test
	public static void testcase2(){
		
		logger = report.startTest("failchecing", "checking browser open with fail");
		driver.get("https://www.facebook.com/");
		
		ByXpath.enterTxt(LoginPage.userName, "suraj@gmail.com", "enter username");
		ByXpath.enterTxt(LoginPage.password, "1234gg", "enter username");
		ByXpath.click(LoginPage.loginBtn, "click on login btn");
		Assert.assertTrue(false);
		
	}
	
	@Test
	public static void testcase3(){
		
		logger = report.startTest("errorchecking", "checking browser open with fail");
		driver.get("https://www.facebook.com/");
		
		ByXpath.enterTxt(LoginPage.userName, "suraj@gmail.com", "enter username");
		ByXpath.enterTxt(LoginPage.password, "1234gg", "enter username");
		ByXpath.click("//*[@id='jhgkgjhf']", "click on login btn");
//		Assert.assertTrue(false);
		
		
	}
}
